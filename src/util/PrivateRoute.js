import { Route, Redirect } from "react-router-dom";
import { useAuth } from "../contexts/AuthContext";
import { TranslateProvider } from "../contexts/TranslateContext";

const PrivateRoute = ({ component: Component, ...rest }) => {
  const { currentUser } = useAuth();

  return (
    <Route
      {...rest}
      render={(props) => {
        return currentUser ? (
          <TranslateProvider>
            <Component {...props} />
          </TranslateProvider>
        ) : (
          <Redirect to="/login" />
        );
      }}
    ></Route>
  );
};
export default PrivateRoute;
