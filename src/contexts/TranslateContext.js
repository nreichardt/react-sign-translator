import { useContext, useState, useEffect, createContext } from "react";
import { useAuth } from "./AuthContext";
import firebase from "firebase/app";
import { db } from "../firebase";

const TranslateContext = createContext();

export const useTranslate = () => {
  return useContext(TranslateContext);
};

export const TranslateProvider = ({ children }) => {
  const { currentUser } = useAuth();
  const [translations, setTranslations] = useState([]);
  const [loading, setLoading] = useState(true);

  const addTranslation = (translation) => {
    return db.collection("userTranslations").add({
      ...translation,
      created: firebase.firestore.Timestamp.fromDate(new Date()),
    });
  };

  const toggleTranslationDeleted = (id) => {
    return db.collection("userTranslations").doc(id).update({
      deleted: true,
    });
  };

  useEffect(() => {
    const unsubscribe = db
      .collection("userTranslations")
      .where("uid", "==", currentUser.uid)
      .where("deleted", "==", false)
      .orderBy("created", "desc")
      .limit(10)
      .onSnapshot((snapshotQuery) => {
        const queriedTranslations = snapshotQuery.docs.map((translation) => ({
          id: translation.id,
          ...translation.data(),
        }));
        setTranslations(queriedTranslations);
        setLoading(false);
      });

    return () => {
      unsubscribe();
    };
  }, [currentUser]);

  const value = {
    translations,
    addTranslation,
    toggleTranslationDeleted,
  };

  return (
    <TranslateContext.Provider value={value}>
      {!loading && children}
    </TranslateContext.Provider>
  );
};
