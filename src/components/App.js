import Signup from "./Signup";
import { Container } from "react-bootstrap";
import { AuthProvider } from "../contexts/AuthContext";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import Profile from "./Profile/Profile";
import Login from "./Login";
import PrivateRoute from "../util/PrivateRoute";
import Header from "./Header";
import Translate from "./Translation/Translate";

function App() {
  return (
    <Container fluid>
      <Router>
        <AuthProvider>
          <Header />

          <Switch>
            <PrivateRoute exact path="/" component={Translate} />
            <PrivateRoute exact path="/Profile" component={Profile} />
            <Route path="/signup" component={Signup} />
            <Route path="/login" component={Login} />
          </Switch>
        </AuthProvider>
      </Router>
    </Container>
  );
}

export default App;
