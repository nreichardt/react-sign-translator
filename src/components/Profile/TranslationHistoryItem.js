import { useState } from "react";
import { ListGroup, Spinner } from "react-bootstrap";
import { XCircle } from "react-bootstrap-icons";
import { useTranslate } from "../../contexts/TranslateContext";

const TranslationHistoryItem = ({ translation, id }) => {
  const { toggleTranslationDeleted } = useTranslate();
  const [loading, setLoading] = useState(false);
  const [error, setError] = useState(null);

  const handleClick = () => {
    try {
      setLoading(true);
      toggleTranslationDeleted(id);
    } catch {
      setError("Failed to delete translation");
    } finally {
      setLoading(false);
    }
  };

  return (
    <>
      {error && <p>{error}</p>}
      {loading && (
        <Spinner animation="border" role="status">
          <span className="sr-only">Loading...</span>
        </Spinner>
      )}
      <ListGroup.Item className="d-flex justify-content-between">
        {translation}
        <XCircle
          className="delTranslation"
          size={25}
          onClick={handleClick}
        ></XCircle>
      </ListGroup.Item>
    </>
  );
};

export default TranslationHistoryItem;
