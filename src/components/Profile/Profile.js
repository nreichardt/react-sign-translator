import { Card, ListGroup, Row, Col } from "react-bootstrap";
import { useTranslate } from "../../contexts/TranslateContext";
import TranslationHistoryItem from "./TranslationHistoryItem";

const Profile = () => {
  const { translations } = useTranslate();

  return (
    <>
      <Row>
        <Col />
        <Col xs={4}>
          <Card>
            <Card.Header>Translation History</Card.Header>
            <ListGroup variant="flush">
              {translations.length > 0 &&
                translations.map((x) => (
                  <TranslationHistoryItem
                    key={x.id}
                    translation={x.msgToTranslate}
                    id={x.id}
                  />
                ))}
            </ListGroup>
          </Card>
        </Col>
        <Col />
      </Row>
    </>
  );
};
export default Profile;
