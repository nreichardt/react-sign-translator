import { useState } from "react";
import {
  Image,
  Alert,
  Col,
  Row,
  InputGroup,
  FormControl,
} from "react-bootstrap";
import { ArrowRightCircleFill, Keyboard } from "react-bootstrap-icons";
import { useAuth } from "../contexts/AuthContext";
import { Link, useHistory } from "react-router-dom";

const Login = () => {
  const [userName, setUserName] = useState("");
  const { login } = useAuth();
  const [error, setError] = useState("");
  const [loading, setLoading] = useState(false);
  const history = useHistory();

  const handleUserNameChange = (event) =>
    setUserName(
      event.target.value + process.env.REACT_APP_FIREBASE_EMAIL.toString()
    );

  async function handleSubmit(e) {
    e.preventDefault();
    try {
      setError("");
      setLoading(true);
      await login(userName);
      history.push("/");
    } catch {
      setError("Account doesn't exist");
    }

    setLoading(false);
  }

  return (
    <>
      <Row className="Input__Row">
        <Col />
        <Col xs={6}>
          <div className="Intro__Logos">
            <Image
              className="intro-splash"
              src={"../../img/Splash.svg"}
            ></Image>
            <Image className="intro-logo" src={"../../img/Logo.png"}></Image>
            <h2>Lost in Translation</h2>
            <h3>Get started</h3>
          </div>
          <div className="Login_Input_Container">
            <div className="Login_Input_Control">
              <InputGroup size="lg">
                <InputGroup.Prepend id="inputGroup-sizing-lg">
                  <Keyboard
                    className="KeyboardIcon"
                    size={48}
                    style={{}}
                  ></Keyboard>
                </InputGroup.Prepend>
                <FormControl
                  className="Input__Field"
                  type="text"
                  onChange={handleUserNameChange}
                  placeholder="What's your name?"
                  aria-label="Large"
                  aria-describedby="inputGroup-sizing-lg"
                  style={{ fontSize: "2em" }}
                />
                <InputGroup.Append>
                  <ArrowRightCircleFill
                    className="SubmitArrow"
                    size={56}
                    onClick={handleSubmit}
                    disabled={loading}
                  />
                </InputGroup.Append>
              </InputGroup>
            </div>
            <div className="signup-text w-100 text-center mt-2">
              {error && <Alert variant="danger">{error}</Alert>}
              Need an account? <Link to="/signup">Sign Up</Link>
            </div>
          </div>
        </Col>
        <Col />
      </Row>
    </>
  );
};
export default Login;
