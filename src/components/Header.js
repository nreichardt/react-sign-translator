import { Dropdown, Image, Navbar } from "react-bootstrap";
import { PersonCircle } from "react-bootstrap-icons";
import { Link, useHistory } from "react-router-dom";
import { useAuth } from "../contexts/AuthContext";
const Header = () => {
  const { currentUser, logout } = useAuth();
  const history = useHistory();

  const handleLogout = async () => {
    try {
      await logout();
      history.push("/login");
    } catch (e) {
      console.log(e.message);
    }
  };

  return (
    <Navbar>
      <Navbar.Brand>
        <Image className="nav-splash" src={"../../img/Splash.svg"}></Image>
        <Image src={"../../img/Logo.png"}></Image>
        <Link to="/">Lost in Translation</Link>
      </Navbar.Brand>
      {currentUser && (
        <Navbar.Collapse className="justify-content-end">
          <div className="header_username">
            <span>
              {currentUser.email.slice(0, currentUser.email.indexOf("@"))}
            </span>
          </div>
          <Dropdown>
            <Dropdown.Toggle variant="warnign" id="dropdown-basic">
              <PersonCircle size={52} color={"white"} />
            </Dropdown.Toggle>

            <Dropdown.Menu>
              <Dropdown.Item>
                <Link to="/profile">Profile</Link>
              </Dropdown.Item>
              <Dropdown.Item>
                <Link to="/">Translator</Link>
              </Dropdown.Item>
              <Dropdown.Item onClick={handleLogout}>Log Out</Dropdown.Item>
            </Dropdown.Menu>
          </Dropdown>
        </Navbar.Collapse>
      )}
    </Navbar>
  );
};

export default Header;
