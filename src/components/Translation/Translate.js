import { useState } from "react";
import { InputGroup, FormControl, Row, Col } from "react-bootstrap";
import { ArrowRightCircleFill, Keyboard } from "react-bootstrap-icons";
import { useAuth } from "../../contexts/AuthContext";
import { useTranslate } from "../../contexts/TranslateContext";
import TranslationItem from "./TranslationItem";

const Translate = () => {
  const [translationInput, setTranslationInput] = useState("");
  const [messageToTranslate, setMessageToTranslate] = useState("");
  const { currentUser } = useAuth();
  const { addTranslation } = useTranslate();

  const handleInputChange = (event) => {
    setTranslationInput(event.target.value);
  };
  const handleSubmit = async (event) => {
    event.preventDefault();
    const translation = {
      msgToTranslate: translationInput,
      deleted: false,
      uid: currentUser.uid,
    };
    try {
      await addTranslation(translation);
    } catch (error) {
      console.log(error);
    }

    setMessageToTranslate(translationInput);
  };

  return (
    <>
      <Row className="Input__Row">
        <Col></Col>
        <Col xs={6}>
          <div className="Input_Control">
            <InputGroup size="lg">
              <InputGroup.Prepend id="inputGroup-sizing-lg">
                <Keyboard
                  className="KeyboardIcon"
                  size={48}
                  style={{}}
                ></Keyboard>
              </InputGroup.Prepend>
              <FormControl
                className="Input__Field"
                type="text"
                onChange={handleInputChange}
                placeholder="Type a word to translate.."
                aria-label="Large"
                aria-describedby="inputGroup-sizing-lg"
                style={{ fontSize: "2em" }}
              />
              <InputGroup.Append>
                <ArrowRightCircleFill
                  className="SubmitArrow"
                  size={56}
                  onClick={handleSubmit}
                />
              </InputGroup.Append>
            </InputGroup>
          </div>
        </Col>
        <Col></Col>
      </Row>
      <Row>
        <Col></Col>
        <Col xs={6}>
          <div className="Translate__Output">
            <TranslationItem wordToTranslate={messageToTranslate} />
          </div>
        </Col>
        <Col></Col>
      </Row>
    </>
  );
};

export default Translate;
