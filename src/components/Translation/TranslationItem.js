import { Image } from "react-bootstrap";

const TranslationItem = ({ wordToTranslate }) => {
  const images = wordToTranslate
    .toLowerCase()
    .replace(/ /g, "")
    .split("")
    .map((x) => (
      <Image
        key={`img-${Math.random()}`}
        src={`../../img/${x}.png`}
        alt={`${x}`}
      ></Image>
    ));
  return <div>{images}</div>;
};

export default TranslationItem;
