# Sign Translator Project

Sign Translator App made with React

The project had 3 page requirements:

### 1. Login page:

- User must be able to enter their name, and login.

### 2. Translation page:

- User may only view this page if they are logged in. Redirect user back to login page if no active login session exists.
- User types in a word to the translation box, and clicks the translate button to trigger the translation.
- Each users translations must be stored

### 3. Profile page:

- Profile page must display the users last 10 translations
- User must be able to delete translations, marking them inactive in the database
- User must be able to log out.

## To Start the App

In the project directory, you run:

### `npm install`

Installs any missing dependencies

### `npm run-script dev`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.\
You will also see any lint errors in the console.
